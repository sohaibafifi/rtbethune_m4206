import 'package:flutter/material.dart';
import 'package:webfeed/webfeed.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_html_view/flutter_html_view.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'RT Béthune',
        home: News()
    );
  }
}


class News extends StatefulWidget {
  @override
  NewsState createState() => new NewsState();
}

class NewsState extends State<News> {


  Future<AtomFeed> getRss() async {
    final response = await http.get('https://rt-bethune.univ-artois.fr/feed.xml');
    return new AtomFeed.parse(response.body);
  }


  Widget createListView(BuildContext context, AsyncSnapshot snapshot) {
    List<AtomItem> items = snapshot.data.items;
    return new ListView.builder(
      itemCount: items.length,
      itemBuilder: (BuildContext context, int index) {
        return new Column(
          children: <Widget>[
            new ListTile(
              title: new Text(items
                  .elementAt(index)
                  .title),
              subtitle: new HtmlView(data: items.elementAt(index).summary),
            ),
            new Divider(height: 2.0,),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    var futureBuilder = new FutureBuilder(
      future: getRss(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.waiting:
            return new Text('loading...');
          default:
            if (snapshot.hasError)
              return new Text('Error: ${snapshot.error}');
            else
              return createListView(context, snapshot);
        }
      },
    );

    return new Scaffold(
      appBar: new AppBar(
        title: new Text("RT Béthune annonces"),
      ),
      body: futureBuilder,
    );
  }
}






